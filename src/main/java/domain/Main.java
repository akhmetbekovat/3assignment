package domain;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        MyApplication application = new MyApplication();
        System.out.println("An application is about to start..");
        application.start();

    }
}
