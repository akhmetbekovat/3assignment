
package domain;


import java.util.List;
import java.util.Scanner;

public class MoreDetails extends User {
    public Scanner sc = new Scanner(System.in);
    private String profession;
    private String maritalStatus;

    //override method of User
    public MoreDetails(String name, String surname, String username, Password passwordStr, String profession, String maritalStatus) {
        super(name, surname, username, passwordStr);
        setProfession(profession);
        setMaritalStatus(maritalStatus);
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getProfession() {
        return profession;
    }

    public void setMaritalStatus(String maritalStatus) {
        System.out.println("1. Single");
        System.out.println("2. Married");
        System.out.println("3. Widowed");
        System.out.println("4. Divorced");
        int choice = sc.nextInt();
        switch (choice){
            case 1:
                maritalStatus="Single";
                this.maritalStatus=maritalStatus;
            case 2:
                maritalStatus="Married";
                this.maritalStatus=maritalStatus;
            case 3:
                maritalStatus="Widowed";
                this.maritalStatus=maritalStatus;
            case 4:
                maritalStatus="Divorced";
                this.maritalStatus=maritalStatus;
            default:
                System.out.println(" ");
        }
    }

    public void getMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    @Override
    public String toString() {
        return super.toString() + " " + profession;
    }
}


