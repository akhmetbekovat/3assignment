package domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {
    public Scanner sc = new Scanner(System.in);
    private User signedUser;
    private ArrayList<User> listOfUsers;//AGGREGATION

    public MyApplication() {
        listOfUsers = new ArrayList<User>();
    }


    private void addUser(User user) {
        listOfUsers.add(user);
    }



    private void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                System.out.println("3. Log out");
                int choice = sc.nextInt();
                switch (choice) {
                    case 1:
                        authentication();
                }
            }
        }
    }
    private void authentication() {
        System.out.println("1. Log in");
        System.out.println("2. Sign up");
        int choice = sc.nextInt();
        if (choice == 1) {
            logIn();
        } else if (choice == 2) {
            signUp();
        }
    }
    private void logIn(){
        System.out.println("Enter your username");
        String username= sc.nextLine();
        System.out.println("Enter your password");
        Password password = sc.nextLine();
        if (!listOfUsers.isEmpty()){
            for (int i=0; i<listOfUsers.size(); i++){
                User user = listOfUsers.get(i);
                if (username.equals(user.getUsername())&&password.equals(password.getPassword)){
                    signedUser=user;
                }else{
                    System.out.println("There does not exist such user");
                }
            }
        }
    }
    private void signUp(){
        try {
            User user = new User();
            System.out.println("Enter your name: ");
            String name= sc.nextLine();
            user.setName(name);
            System.out.println("Enter your surname: ");
            String surname= sc.nextLine();
            user.setSurname(surname);
            System.out.println("Enter your username: ");
            String username = sc.nextLine();
            System.out.println("Enter your password: ");
            Password password = sc.nextLine();
            for (int i=0; i<listOfUsers.size(); i++){
                User users = listOfUsers.get(i);
                if(username.equals(user.getUsername())&& password.equals(user.getPasswordStr())) {
                    user.setUsername(username);
                    user.setPasswordStr(password);
                }
            }
            System.out.println("Created successfully");
        }catch (Exception e){
            System.out.println("Try again");
            signUp();
        }
    }

    private void logOff() {
        signedUser = null;
    }


    public void start() throws FileNotFoundException {
        File file = new File("C:\\Users\\Нурболат\\Desktop\\TOMA\\Java OOP\\application\\db.txt");
        Scanner fileScanner = new Scanner(file);
        while (fileScanner.hasNextLine()) {
            String data = fileScanner.nextLine();
            System.out.println(data);
        }
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }
    }

    private void saveUserList() {
        try (FileWriter f1 = new FileWriter("db.txt")) {
            for (int i = 0; i < listOfUsers.size(); i++) {
                f1.write(String.valueOf(listOfUsers.get(i)) + "/n");
                f1.flush();
                f1.close();
                System.out.println("Successfully saved!");
            }
        } catch (Exception e) {
            System.out.println("Error");
            e.printStackTrace();
        }
    }
}


