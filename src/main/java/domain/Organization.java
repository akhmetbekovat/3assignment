package domain;

import java.util.List;

public class Organization {
    private String name;
    private int existence;
    private List<MoreDetails> employees;//composition

    public String getName() {
        return name;
    }

    public int getExistence() {
        return existence;
    }

    public List<MoreDetails> getEmployees() {
        return employees;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExistence(int existence) {
        this.existence = existence;
    }

    public void setEmployees(List<MoreDetails> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "name='" + name + '\'' +
                ", existence=" + existence +
                ", employees=" + employees +
                '}';
    }
}
