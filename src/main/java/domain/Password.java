package domain;


public class Password {
    private String password;

    public Password(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public boolean setPassword(String newPassword) {
        if (isValid(newPassword)) {
            this.password = newPassword;
        } else System.out.println("Your password is invalid!");
        return false;
    }

    private boolean isValid(String password) {
        boolean hasNum = false; boolean hasCap=false; boolean hasLow= false;
        for (int i=0; i<password.length(); i++){
            if (Character.isDigit(password.charAt(i))){
                hasNum=true;
            }
            if (Character.isUpperCase(password.charAt(i))){
                hasCap=true;
            }
            if (Character.isLowerCase(password.charAt(i))){
                hasLow=true;
            }
            if (password.length()<=9 && hasNum && hasCap && hasLow){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return password;
    }
}

