package domain;


import java.util.List;

public class User {
    private int id;
    private static int id_gen = 0;
    private String name;
    private String surname;
    private String username;
    private Password passwordStr;



    public User() {

    }



    public User(String name, String surname, String username, Password passwordStr) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.passwordStr = passwordStr;
        generateID();
    }

    public void generateID() {
        id = id_gen++;
    }

    public int getId() {
        return id;
    }

    public static int getId_gen() {
        return id_gen;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public Password getPasswordStr() {
        return passwordStr;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static void setId_gen(int id_gen) {
        User.id_gen = id_gen;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPasswordStr(Password passwordStr) {
        this.passwordStr = passwordStr;
    }

    @Override
    public String toString() {
        return name + " " + surname + " " + username + " " + passwordStr;
    }
}
